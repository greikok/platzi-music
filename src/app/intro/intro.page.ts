import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    centerdSlides: true,
    speed: 400
  };

  slides = [{
    title: 'Este es el titulo' ,
    subtitle: 'este es el subtitulo',
    description: 'aqui va la descripcion',
    icon: 'play'
    },
    {},
    {}
    ];

  constructor() { }

  ngOnInit() {
  }

}
